<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/test','ExampleController@test');
$router->get('/testflush','ExampleController@testflush');
$router->get('/testget','ExampleController@testget');

$router->post('/SignIn','Auth\AuthController@SignIn');
$router->group(['middleware' => 'jwt.auth'], 
function() use ($router) {
    $router->get('/SignOut','Auth\AuthController@SignOut');
    
    // Settings
    $router->post('/Settings/Menu/Datatable','Settings\MenuController@Datatable');
});