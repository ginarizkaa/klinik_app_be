<?php

namespace App\Repositories\Auth;

use App\Models\Tables\sys_menus;
use App\Models\Tables\sys_signin_activity;
use App\Models\Tables\tb_users;

class AuthRepository {

    public function SignIn($id, $attr)
    {
        return tb_users::with(['role'])->where([
                ['user_id', '=', $id],
                ['user_attr', '=', $attr],
            ])->get()->first();
    }

    public function GetMenus($role_id)
    {
        $res = sys_menus::whereHas('menu_access', function ($query) use ($role_id) { $query->where('sys_roles_menus.role_id', '=', $role_id); })
                ->get();
        // $res = sys_menus::with(['menu_access' => fn ($query) => $query->where('sys_roles_menus.role_id', '=', $role_id)])
        //         ->whereHas('menu_access', function ($query) use ($role_id) { $query->where('sys_roles_menus.role_id', '=', $role_id); })
        //         ->get();
        return $res;
    }

    public function SignLog($id, $token = NULL)
    {
        return sys_signin_activity::updateOrCreate(
                ['user_id'       => $id],
                [
                    'access_token'  => $token,
                    'ip_address'    => request()->ip()
                ]
        );
    }

}

?>