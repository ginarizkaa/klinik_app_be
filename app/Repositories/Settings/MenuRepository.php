<?php

namespace App\Repositories\Settings;

use Illuminate\Support\Facades\DB;

class MenuRepository {

    public function GetDatatable($limit, $page, $order, $filters)
    {
        $result = array();

        $query = DB::table('sys_menus');

        
        if($filters) {
            foreach ($filters as $key => $value) {
                $query->where($key, 'like',  "%".$value."%");
            }
        }
        
        for($i=0;$i<count($order);$i++) {
            $query->orderBy($order[$i]['field'], $order[$i]['type']);
        }
        
        $query->paginate($limit, ['*'], 'page', $page);
        
        $result['data']     = $query->get();
        $result['total']    = $query->count();

        return $result;
    }

}

?>