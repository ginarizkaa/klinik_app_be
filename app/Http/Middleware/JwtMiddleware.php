<?php

namespace App\Http\Middleware;

use App\Models\Tables\sys_signin_activity;
use App\Models\Tables\tb_users;
use App\Resources\Enums\RESULT;
use App\Resources\Ss;
use Closure;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class JwtMiddleware
{
    public function handle($request, Closure $next, $guard = null)
    {
        $token= request()->bearerToken();
        
        if(!$token) {
            return response()->json([
                "status"    => RESULT::FAILED_400['status'],
                "code"      => RESULT::FAILED_400['code'],
                "message"   => "Unauthorized [A].",
            ]);
        }
        try {
            if($token != Ss::retrieves('access_token')) {
                $check = sys_signin_activity::where('user_id', Ss::retrieves('user_id'))
                                        ->orWhere('access_token', $token)
                                        ->first();
                if($check['access_token'] != $token) {
                    return response()->json([
                        "status"    => RESULT::FAILED_401['status'],
                        "code"      => RESULT::FAILED_401['code'],
                        "message"   => "Unauthorized [B].",
                    ]);
                } else {
                    JWT::decode($token, new Key(env('JWT_SECRET'),'HS256'));
                    $data_user = tb_users::where('user_id', $check['user_id'])->first();
                    foreach (json_decode($data_user) as $key => $val) {
                        Ss::stores($key, $val);
                    }
                    Ss::stores('access_token', $token);
                }
            } else {
                JWT::decode($token, new Key(env('JWT_SECRET'),'HS256'));
            }
            return $next($request);
        } catch(ExpiredException $e) {
            return response()->json([
                "status"    => RESULT::FAILED_401['status'],
                "code"      => RESULT::FAILED_401['code'],
                "message"   => "Unauthorized. Token expired.",
            ]);
        } 
        catch (\Exception $e) {
            return response()->json([
                "status"    => RESULT::ERROR_500['status'],
                "code"      => RESULT::ERROR_500['code'],
                "message"   => $e->getMessage(),
            ]);
        }

    }
}