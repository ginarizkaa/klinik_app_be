<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Resources\Enums\RESULT;
use App\Resources\Exceptions\InvalidException;
use App\Services\Auth\AuthServices;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->service = new AuthServices;
    }

    public function SignIn(Request $request)
    {
        try {
            $data = $request->only([
                'user_id','user_pass'
            ]);
            $res = $this->service->SignInService($data);

            return $this->returnResponse(RESULT::SUCCESS_200, "Login Success.", $res);
        } 
        catch (InvalidException $e) {
            return $this->returnResponse(
                $e->errorStatusCode(), $e->errorMessage(), $e->errorData());
        } 
        catch (\Exception $e) {
            return $this->errorResponse(RESULT::ERROR_500, $e->getMessage());
        }
        
    }

    public function SignOut()
    {
        try {
            $res = $this->service->SignOutService();
            return $this->returnResponse(RESULT::SUCCESS_200, "Sign Out Success.", $res);
        } 
        catch (InvalidException $e) {
            return $this->returnResponse(
                $e->errorStatusCode(), $e->errorMessage(), $e->errorData());
        } 
        catch (\Exception $e) {
            return $this->errorResponse(RESULT::ERROR_500, $e->getMessage());
        }
    }

}
