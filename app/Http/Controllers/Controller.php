<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use App\Resources\Ss;

class Controller extends BaseController
{
    public function returnResponse($status_code, $message, $data = NULL, $line = NULL)
    {
        return response()->json([
            "status"    => $status_code['status'],
            "code"      => $status_code['code'],
            "message"   => $message,
            "data"      => $data
        ]);
    }

    public function errorResponse($status_code, $message, $file = NULL, $line = NULL)
    {
        return response()->json([
            "status"    => $status_code['status'],
            "code"      => $status_code['code'],
            "message"   => $message,
            "file"      => $file,
            "line"      => $line,
        ]);
    }
}
