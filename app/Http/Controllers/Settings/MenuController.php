<?php

namespace App\Http\Controllers\Settings;

use App\Http\Controllers\Controller;
use App\Resources\Enums\RESULT;
use App\Resources\Exceptions\InvalidException;
use App\Services\Settings\MenuServices;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    public function __construct()
    {
        $this->service = new MenuServices;
    }

    public function Datatable(Request $request)
    {
        try {
            $data = $request->all();
            $res = $this->service->DatatableServices($data);

            return $this->returnResponse(RESULT::SUCCESS_200, "Success.", $res);
        } 
        catch (InvalidException $e) {
            return $this->returnResponse(
                $e->errorStatusCode(), $e->errorMessage(), $e->errorData());
        } 
        catch (\Exception $e) {
            return $this->errorResponse(RESULT::ERROR_500, $e->getMessage());
        }
    }
        
}
