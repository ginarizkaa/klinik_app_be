<?php

namespace App\Http\Controllers;

use App\Resources\Ss;

class ExampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function test()
    {
        $store = Ss::stores('user', ['name' => 'gina']);
        echo $store;
    }

    public function testget()
    {
        echo "test";
    }

    public function testflush()
    {
        $retrieve =  Ss::flushes();
        echo $retrieve;
    }

    public function midd()
    {
        echo "yes";
    }
}
