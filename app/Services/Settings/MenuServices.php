<?php

namespace App\Services\Settings;

use App\Repositories\Settings\MenuRepository;
use App\Resources\Exceptions\InvalidException;
use Exception;

class MenuServices {
    
    public function __construct()
    {
        $this->repo = new MenuRepository();
    }

    public function DatatableServices($data)
    {
        try {
            $limit      = $data['perPage'];
            $page       = $data['page'];
            $order      = $data['sort'];
            $filter     = $data['filters'];

            $result = $this->repo->GetDatatable($limit, $page, $order, $filter);
            
            return $result;
        } catch (InvalidException $e) {
            throw new InvalidException($e->errorStatusCode(), $e->errorMessage(), $e->errorData());
        } 
        catch (\Exception $e) {
            throw new Exception($e->getMessage(), $e->getLine());
        }
        
    }

}

?>