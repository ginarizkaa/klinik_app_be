<?php

namespace App\Services\Auth;

use App\Repositories\Auth\AuthRepository;
use App\Resources\Enums\RESULT;
use App\Resources\Exceptions\InvalidException;
use App\Resources\Ss;
use App\Services\Utils\ValidationService;
use Exception;
use Firebase\JWT\JWT;

class AuthServices {
    
    public function __construct()
    {
        $this->repo     = new AuthRepository();
        $this->service  = new ValidationService();
    }

    public function SignInService($data)
    {
        try {
            $rules = [
                'user_id' => 'required',
                'user_pass' => 'required'
            ];
            $this->service->manualValidation($data, $rules);

            $result['data_user'] = $this->repo->SignIn($data['user_id'],$data['user_pass']);
            if (empty($result['data_user'])) {
                throw new InvalidException(RESULT::FAILED_400, "Incorrect Username or Password.");
            }

            $getMenus = $this->repo->GetMenus($result['data_user']['role_id']);
            $result['data_menu'] = $this->_arrangeMenus($getMenus);

            $payload = [
                'key' => $this->_randomString(5),
                'iss' => 'klinik_app', 
                'sub' => 'trial', 
                'iat' => time(), 
                'exp' => time() + 60*60, 
                'usr' => $result['data_user']['user_id'],
            ];
            $result['access_token'] = JWT::encode($payload, env('JWT_SECRET'), ['HS256']);

            $this->repo->SignLog($result['data_user']['user_id'],$result['access_token']);
            
            // storing session
            foreach (json_decode($result['data_user']) as $key => $val) {
                Ss::stores($key, $val);
            }
            Ss::stores('access_token', $result['access_token']);

            return $result;
        } catch (InvalidException $e) {
            throw new InvalidException($e->errorStatusCode(), $e->errorMessage(), $e->errorData());
        } 
        catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
        
    }

    public function SignOutService()
    {
        try {
            $this->repo->SignLog(Ss::retrieves('user_id'));
            
            // flushing session
            Ss::flushes();

            return;
        } catch (InvalidException $e) {
            throw new InvalidException($e->errorStatusCode(), $e->errorMessage(), $e->errorData());
        } 
        catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
    
    private function _randomString($count = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';
        for ($i = 0; $i < $count; $i++) {
            $randstring .= $characters[rand(0, strlen($characters))];
        }
        return $randstring;
    }

    private function _arrangeMenus($data) {
        $res = [];
        foreach ($data as $item) {
            if ($item['menu_parent_id'] == null) {
                $filtered = array_filter($data->toArray(), function($val, $key) use ($item) {
                    return $val['menu_parent_id'] == $item['menu_id'];
                }, ARRAY_FILTER_USE_BOTH);
                if ($filtered != []) {
                    unset($item['menu_access']);
                }
                $item['menu_child'] = array_values($filtered);
                $res[] = $item;
            }
        }
        return $res;
    }
}

?>