<?php

namespace App\Services\Utils;

use App\Resources\Enums\RESULT;
use App\Resources\Exceptions\InvalidException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ValidationService
{
    public function manualValidation($data, $rules)
    {
        $validator = Validator::make($data, $rules, $this->messages(), $this->attributes());
        if ($validator->fails()) {
            throw new InvalidException(RESULT::FAILED_202, "Validation Error.", $validator->errors());
        }
    }

    public function autoValidation($data, $table_name)
    {
        $sql    = DB::table('sys_master_validation')
                    ->where('table_name', '=', $table_name)
                    ->select(['field_name', 'rules'])
                    ->get();
        $res    = json_decode($sql, true);

        $rules  = array();
        foreach ($res as $val) {
            $rules[$val["field_name"]] = $val["rules"];
        }

        $this->manualValidation($data, $rules);        
    }

    private function messages()
    {
        return [
            'required'      =>  ':attribute is required.'
        ];
    }

    private function attributes()
    {
        return [
            'user_name'     => 'Username',
            'user_pass'     => 'Password'
        ];
    }
}