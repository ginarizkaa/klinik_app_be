<?php

namespace App\Resources\Exceptions;

use Exception;

class InvalidException extends Exception
{
    public function __construct($status_code, $message, $data = null)
    {
        $this->status_code  = $status_code;
        $this->message      = $message;
        $this->data         = $data;
    }

    public function errorStatusCode()
    {
        return $this->status_code;
    }

    public function errorMessage()
    {
        return $this->message;
    }

    public function errorData()
    {
        return $this->data;
    }

}