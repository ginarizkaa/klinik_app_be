<?php

namespace App\Resources;

use Illuminate\Support\Facades\Session;

class Ss
{
    public static function stores($key, $value)
    {
        return Session::put([$key => $value]);
    }
    
    public static function retrieves($key)
    {
        return Session::get($key);
    }
    
    public static function flushes()
    {
        return Session::flush();
    }
}