<?php
namespace App\Resources\Enums;

class RESULT 
{

    // SUCCESS
    const SUCCESS_200   = array('status' => 'SUCCESS', 'code' => 200);

    // VALIDATION ERROR
    const FAILED_202    = array('status' => 'FAILED', 'code' => 202);

    // BAD REQUEST
    const FAILED_400    = array('status' => 'FAILED', 'code' => 400);

    // UNAUTHORIZED
    const FAILED_401    = array('status' => 'FAILED', 'code' => 401);

    // NOT FOUND
    const ERROR_404     = array('status' => 'ERROR', 'code' => 404);

    // SERVER ERROR
    const ERROR_500     = array('status' => 'INTERNAL_SERVER_ERROR', 'code' => 500);

}