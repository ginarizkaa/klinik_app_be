<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->loadHelpers();
    }

    protected function loadHelpers()
    {
        require_once __DIR__ . '/../Resources/SessionsResource.php';

        // if there is folder full of  helpers
        // foreach (glob(__DIR__ . '/../Helpers/*.php') as $filename) {
        //     require_once $filename;
        // }
    }
    
}
