<?php

namespace App\Models\Tables;

use App\Models\ParentModel;

class tb_users extends ParentModel
{
    protected $table        = 'tb_users';

    protected $primaryKey   = 'user_id';
    protected $keyType      = 'string';
    public $incrementing    = false;

    protected $fillable     = [
        'user_id',
        'user_fullname',
        'user_attr',
        'active_flag',
        'created_by',
        'created_date',
        'update_by',
        'update_date',
        'delete_flag'
    ];

    protected $hidden = [
        'role_id',
        'user_attr',
        'created_by',
        'created_date',
        'update_by',
        'update_date'
    ];

    public function role()
    {
        return $this->hasOne(sys_roles::class, 'role_id', 'role_id');
    }
}