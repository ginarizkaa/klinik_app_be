<?php

namespace App\Models\Tables;

use App\Models\ParentModel;

class sys_menus extends ParentModel
{
    protected $table        = 'sys_menus';

    protected $fillable     = [
        'menu_parent_id',
        'menu_name',
        'menu_icon',
        'menu_url',
    ];

    public function menu_access()
    {
        return $this->hasOne(sys_roles_menus::class, 'menu_id', 'menu_id');
    }
}