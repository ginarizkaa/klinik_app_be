<?php

namespace App\Models\Tables;

use App\Models\ParentModel;

class sys_roles extends ParentModel
{
    protected $table        = 'sys_roles';

    protected $primaryKey   = 'role_id';
    protected $keyType      = 'string';
    public $incrementing    = false;

    protected $fillable     = [
        'role_name',
        'default_menu'
    ];

    public function user()
    {
        return $this->belongsTo(tb_users::class, 'role_id', 'role_id');
    }
}