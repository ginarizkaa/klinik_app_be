<?php

namespace App\Models\Tables;

use App\Models\ParentModel;

class sys_signin_activity extends ParentModel
{
    protected $table        = 'sys_signin_activity';

    protected $primaryKey   = 'user_id';

    public $timestamps      = false;
    
    protected $fillable     = [
        'user_id',
        'access_token',
        'ip_address'
    ];
}