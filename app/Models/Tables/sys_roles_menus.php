<?php

namespace App\Models\Tables;

use App\Models\ParentModel;

class sys_roles_menus extends ParentModel
{
    protected $table        = 'sys_roles_menus';

    protected $fillable     = [
        'role_id',
        'menu_id',
        'act_add',
        'act_view',
        'act_edit',
        'act_delete',
    ];

    protected $hidden = [
        'role_id',
        'menu_id',
    ];

    public function menus()
    {
        return $this->belongsTo(sys_master_menu::class);
    }

}