<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParentModel extends Model {

    // timestamps modified column name
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'update_date';

}